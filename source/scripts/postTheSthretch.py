#!/usr/bin/python3
# coding=utf-8

# theauldsthretch -- Posting the length of the grand auld stretch in
#                   the evening all year 'round.

# Copyright 2017-2019 Éibhear Ó hAnluain

# This file is part of theauldsthretch.
#
# theauldsthretch is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# theauldsthretch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# SPDX-FileCopyrightText: 2017-2019 Éibhear Ó hAnluain <eibhear.geo@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import time
import logging
import os
import configparser
import requests

from twython import Twython
from datetime import datetime as dt
from datetime import timedelta as td
from datetime import timezone as tz
from crontab import CronTab

# A dictionary that will contain all the configuration settings, both
# from the config file and from the environment.
runConfig = {}

#
# buildRunConfig -- To build up the runConfig dictionary from the
#                   settings in the configuratoin file and the
#                   environment.
#
# parameters:
#   thisScript -- The full path to the script for which the call is
#                 being made. Normally this actual script, but useful
#                 for testing.
#
# return: Nothing
#
# TODOs:
#   - Maintain a list of the expected settings, and what their
#     datatypes should be
#   - Build a set of sanity checks on config settings
#
def buildRunConfig(thisScript):
    global runConfig
    generalDict = {}
    featuresDict = {}
    databaseDict = {}
    twitterDict = {}
    mastodonDict = {}
    gnusocialDict = {}
    friendicaDict = {}
    blueskyDict = {}
    debuggingDict = {}
    messagesDict = {}

    mainConfig=configparser.RawConfigParser()

    generalDict["fullCommand"] = thisScript
    generalDict["appName"] = 'theauldsthretch'
    generalDict["scriptBaseName"]=\
        os.path.basename(os.path.splitext(thisScript)[0])

    generalDict["scriptDir"]=os.path.dirname(thisScript)
    generalDict["instDir"]=os.path.dirname(generalDict["scriptDir"])
    generalDict["configDir"]=generalDict["instDir"] + '/config'

    # Read the config file
    mainConfig.read( generalDict["configDir"] + '/' +
                     generalDict["appName"] + '.config')

    generalDict["filesDir"]=generalDict["instDir"] + '/' + \
        mainConfig.get('general', 'filesDir')
    generalDict["logDir"]=generalDict["instDir"] + '/' + \
        mainConfig.get('general', 'logDir')

    generalDict["operatingYear"] = mainConfig.get('general', 'operatingYear')

    generalDict["messagingSystems"] = \
        mainConfig.get('general', 'messagingSystems')

    generalDict["logLevel"] =  mainConfig.get('general', 'logLevel')
    generalDict["logFile"] = generalDict["logDir"] + '/' + \
        mainConfig.get('general','logFile')

    generalDict["retries"] =  int ( mainConfig.get('general', 'retries') )
    generalDict["catchupWait"] =  \
        int ( mainConfig.get('general', 'catchupWait') )

    generalDict["yearDBFileName"] = generalDict['operatingYear'] + '_db.txt'
    generalDict["yearDBFileBackupName"] = \
        generalDict["yearDBFileName"].replace('_db', '_db_backup')
    generalDict["nowDate"] = dt.date ( dt.today() )

    runConfig["general"] = generalDict

    logging.basicConfig ( filename=runConfig["general"]["logFile"],
                          format='"%(asctime)s","%(name)s",' + \
                          '"%(levelname)s","%(funcName)s","%(message)s"',
                          datefmt='%Y-%m-%d %H:%M:%S',
                          level=runConfig["general"]['logLevel']
    )
    logging.info ( "Logging initiated during buildRunConfig(" + \
                   thisScript + ")" )

    featuresDict["feature_main"] = 'True'
    for theFeature in mainConfig.items('features'):
        featuresDict[theFeature[0]] = theFeature[1]

    runConfig["features"] = featuresDict

    logging.debug ( 'Features dictionary: ' + str(runConfig["features"]) )

    databaseDict["city_name"] = mainConfig.get('database', 'city_name')
    databaseDict["timeFMT"] = mainConfig.get('database', 'timeFMT')

    runConfig["database"] = databaseDict

    twitterDict["apiKey"] = mainConfig.get('twitter', 'apiKey')
    twitterDict["apiSecret"] = mainConfig.get('twitter', 'apiSecret')
    twitterDict["accessToken"] = mainConfig.get('twitter', 'accessToken')
    twitterDict["accessTokenSecret"] = \
        mainConfig.get('twitter', 'accessTokenSecret')

    runConfig["twitter"] = twitterDict

    mastodonDict["mastodonClientKey"] = mainConfig.get('mastodon', 'mastodonClientKey')
    mastodonDict["mastodonClientSecret"] = mainConfig.get('mastodon', 'mastodonClientSecret')
    mastodonDict["mastodonAccessToken"] = mainConfig.get('mastodon', 'mastodonAccessToken')
    mastodonDict["mastodonApiBaseUrl"] = \
        mainConfig.get('mastodon', 'mastodonApiBaseUrl')

    runConfig["mastodon"] = mastodonDict

    gnusocialDict["gnusocialAddress"] = \
        mainConfig.get('gnusocial', 'gnusocialAddress')
    gnusocialDict["gnusocialUser"] = \
        mainConfig.get('gnusocial', 'gnusocialUser')
    gnusocialDict["gnusocialUserPassword"] = \
        mainConfig.get('gnusocial', 'gnusocialUserPassword')

    runConfig["gnusocial"] = gnusocialDict

    friendicaDict["friendicaAddress"] = \
        mainConfig.get('friendica', 'friendicaAddress')
    friendicaDict["friendicaUser"] = \
        mainConfig.get('friendica', 'friendicaUser')
    friendicaDict["friendicaUserPassword"] = \
        mainConfig.get('friendica', 'friendicaUserPassword')

    runConfig["friendica"] = friendicaDict

    blueskyDict["blueskyAddress"] = \
        mainConfig.get('bluesky', 'blueskyAddress')
    blueskyDict["blueskyUser"] = \
        mainConfig.get('bluesky', 'blueskyUser')
    blueskyDict["blueskyUserPassword"] = \
        mainConfig.get('bluesky', 'blueskyUserPassword')

    runConfig["bluesky"] = blueskyDict

    debuggingDict["setCron"] = mainConfig.getboolean('debugging', 'setCron')
    debuggingDict["postMessage"] = \
        mainConfig.getboolean('debugging', 'postMessage')
    debuggingDict["updateDatabase"] = \
        mainConfig.getboolean('debugging', 'updateDatabase')
    debuggingDict["noChanges"] = \
        mainConfig.getboolean('debugging', 'noChanges')

    runConfig["debugging"] = debuggingDict

    messagesDict["catchup"] = mainConfig.get('messages', 'catchup')
    messagesDict["normal"] = mainConfig.get('messages', 'normal')

    runConfig["messages"] = messagesDict

    if runConfig['debugging']['noChanges']:
        logging.info ( "Full debugging turned on in config file." )
        runConfig['debugging']['setCron'] = False
        runConfig['debugging']['updateDatabase'] = False
        runConfig['debugging']['postMessage'] = False

    logging.debug ( 'DB file: ' + runConfig["general"]["filesDir"] + '/' +
                    runConfig["general"]["yearDBFileName"] )
    logging.debug ( 'Backup DB file: ' + runConfig["general"]["filesDir"] +
                    '/' + runConfig["general"]["yearDBFileBackupName"] )
# End buildRunConfig()

#
# featureOn -- To determine whether a specific feature is turned
#              on or not.
#
# parameters:
#   theFeature -- the feature we're querying for
#
# return: True if the feature is turned on. False otherwise.
#
def featureOn(theFeature):

    logging.info ( "Checking if feature '" + theFeature + "' is turned on." )
    if runConfig["features"].get(theFeature) is None:
        logging.warning ( "Checking if feature '" + theFeature + "' is on, but it isn't registered as a feature!" )
        return False
    else:
        if runConfig["features"][theFeature] == "True":
            return True
        else:
            return False

def postTotwitter(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Twitter, but was told not to post at all.")
        return True

    logging.info ( "Posting to Twitter." )
    triesToGo = int ( runConfig["general"]["retries"] )
    api = Twython ( runConfig["twitter"]["apiKey"],
                    runConfig["twitter"]["apiSecret"],
                    runConfig["twitter"]["accessToken"],
                    runConfig["twitter"]["accessTokenSecret"] )
    while triesToGo > 0:
        logging.debug ( "Retries left: " + str ( triesToGo ) )
        twitterUpdate = api.update_status ( status=messageToPost )
        logging.info ( "Attempts left: " + str(triesToGo) )

        if twitterUpdate is not None:
            logging.info ( "Twitter response ID: " + twitterUpdate["id_str"] )
            logging.debug ( "Twitter response: " + str ( twitterUpdate ) )
            return True
        else:
            logging.warning ( "Problem posting to twitter" )
            triesToGo = triesToGo -1

    return False
# End of postTotwitter

def postTomastodon(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Mastodon, but was told not to post at all.")
        return True

    logging.info ( "Posting to Mastodon." )
    from mastodon import Mastodon, MastodonError
    triesToGo = int ( runConfig["general"]["retries"] )
    api = Mastodon ( client_id = runConfig["mastodon"]["mastodonClientKey"],
                     client_secret = runConfig["mastodon"]["mastodonClientSecret"],
                     access_token = runConfig["mastodon"]["mastodonAccessToken"],
                     api_base_url = runConfig["mastodon"]["mastodonApiBaseUrl"] )
    while triesToGo > 0:
        logging.debug ( "Retries left: " + str ( triesToGo ) )
        mastodonUpdate = None
        try:
            mastodonUpdate = api.toot ( messageToPost )
        except MastodonError as MOops:
            logging.warning ("Mastodon Error")
            logging.warning (MOops)
        logging.info ( "Attempts left: " + str(triesToGo) )

        if mastodonUpdate is not None:
            logging.info ( "Mastodon response ID: " + str ( mastodonUpdate["id"] ) )
            logging.debug ( "Mastodon response: " + str ( mastodonUpdate ) )
            return True
        else:
            logging.warning ( "Problem posting to Mastodon" )
            triesToGo = triesToGo -1

    return False
# End of postTomastodon

def postTognusocial(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to GNU Social, but was told not to post at all.")
        return True

    logging.info ( "Posting to GNUSocial." )
    gsa = runConfig["gnusocial"]["gnusocialAddress"]
    gsu = runConfig["gnusocial"]["gnusocialUser"]
    gsup = runConfig["gnusocial"]["gnusocialUserPassword"]

    triesToGo = int ( runConfig["general"]["retries"] )

    while triesToGo > 0:
        # Send the message and capture any error.
        socialResponse = None
        try:
            socialResponse = \
                requests.post ( gsa + 'api/statuses/update.xml',
                                auth=( gsu, gsup ),
                                data={ 'status' :
                                       messageToPost }
                )
        except requests.exceptions.ConnectionError as CEOops:
            logging.warning ("Connection Error")
            logging.warning (CEOops)
        except requests.exceptions.Timeout as TOops:
            logging.warning ("Timeout")
            logging.warning (TOops)
        except requests.exceptions.HTTPError as HEOops:
            logging.warning ("HTTPError")
            logging.warning (HEOops)
        except requests.exceptions.TooManyRedirects as TMROops:
            logging.warning ("TooManyRedirects")
            logging.warning (TMROops)
        except requests.exceptions.RequestException as ROops:
            logging.warning ("Generic RequestException")
            logging.warning (ROops)

        # Log the resonse
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Posting to " +
                              gsa +
                              " as " +
                              gsu +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTognusocial

def postTofriendica(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Friendica, but was told not to post at all.")
        return True

    logging.info ( "Posting to friendica." )
    fsa = runConfig["friendica"]["friendicaAddress"]
    fsu = runConfig["friendica"]["friendicaUser"]
    fsup = runConfig["friendica"]["friendicaUserPassword"]
    logging.debug ( "Address: " + fsa + "; User: " + fsu + "; Password: ????" )

    triesToGo = int ( runConfig["general"]["retries"] )

    while triesToGo > 0:
        # Send the message and capture any error.
        socialResponse = None
        try:
            socialResponse = \
                requests.post ( fsa + 'api/statuses/update.xml',
                                auth=( fsu, fsup ),
                                data={ 'status' :
                                       messageToPost }
                )
        except requests.exceptions.ConnectionError as CEOops:
            logging.warning ("Connection Error")
            logging.warning (CEOops)
        except requests.exceptions.Timeout as TOops:
            logging.warning ("Timeout")
            logging.warning (TOops)
        except requests.exceptions.HTTPError as HEOops:
            logging.warning ("HTTPError")
            logging.warning (HEOops)
        except requests.exceptions.TooManyRedirects as TMROops:
            logging.warning ("TooManyRedirects")
            logging.warning (TMROops)
        except requests.exceptions.RequestException as ROops:
            logging.warning ("Generic RequestException")
            logging.warning (ROops)

        # Log the resonse
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Posting to " +
                              fsa +
                              " as " +
                              fsu +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTofriendica

def createBlueskySession():

    logging.info ( "Creating a Bluesky session." )
    bsa = runConfig["bluesky"]["blueskyAddress"]
    bsu = runConfig["bluesky"]["blueskyUser"]
    bsup = runConfig["bluesky"]["blueskyUserPassword"]
    logging.debug ( "Address: " + bsa + "; User: " + bsu + "; Password: ????" )

    sessionRequestResponse = None
    try:
        sessionRequestResponse = \
            requests.post ( bsa + 'xrpc/com.atproto.server.createSession',
                            json={ "identifier":bsu, "password":bsup },
            )
    except requests.exceptions.ConnectionError as CEOops:
        logging.warning ("Bluesky session creation: Connection Error")
        logging.warning (CEOops)
    except requests.exceptions.Timeout as TOops:
        logging.warning ("Bluesky session creation: Timeout")
        logging.warning (TOops)
    except requests.exceptions.HTTPError as HEOops:
        logging.warning ("Bluesky session creation: HTTPError")
        logging.warning (HEOops)
    except requests.exceptions.TooManyRedirects as TMROops:
        logging.warning ("Bluesky session creation: TooManyRedirects")
        logging.warning (TMROops)
    except requests.exceptions.RequestException as ROops:
        logging.warning ("Bluesky session creation: Generic RequestException")
        logging.warning (ROops)

    # Log the response
    if sessionRequestResponse is not None:
        logging.info (sessionRequestResponse.status_code)
        logging.debug (sessionRequestResponse.headers)
        logging.debug (sessionRequestResponse.text)

    # The response may not have resulted in an error and yet not
    # have succeeded. We are successful only if we get a HTTP 200
    # response.
    if sessionRequestResponse is not None and \
       sessionRequestResponse.status_code == 200:
        return sessionRequestResponse.json()
    else:
        return None
# End of createBlueskySession

def postTobluesky(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Bluesky, but was told not to post at all.")
        return True

    logging.info ( "Posting to bluesky." )
    bsa = runConfig["bluesky"]["blueskyAddress"]
    logging.debug ( "Address: " + bsa )

    triesToGo = int ( runConfig["general"]["retries"] )

    # Create a login session and capture any error that may arise.
    blueskySession = createBlueskySession()

    blueskyPost = { "$type": "app.bsky.feed.post",
                    "text": messageToPost,
                    "createdAt": dt.now(tz.utc).isoformat().replace("+00:00","Z"),
    }

    while triesToGo > 0:

        socialResponse = None
        if blueskySession is not None:
            try:
                socialResponse = \
                    requests.post ( bsa + 'xrpc/com.atproto.repo.createRecord',
                                    headers={ "Authorization": "Bearer " +
                                              blueskySession["accessJwt"]
                                             },
                                    json={ "repo":blueskySession["did"],
                                           "collection":"app.bsky.feed.post",
                                           "record": blueskyPost, },
                    )
            except requests.exceptions.ConnectionError as CEOops:
                logging.warning ("Bluesky session creation: Connection Error")
                logging.warning (CEOops)
            except requests.exceptions.Timeout as TOops:
                logging.warning ("Bluesky session creation: Timeout")
                logging.warning (TOops)
            except requests.exceptions.HTTPError as HEOops:
                logging.warning ("Bluesky session creation: HTTPError")
                logging.warning (HEOops)
            except requests.exceptions.TooManyRedirects as TMROops:
                logging.warning ("Bluesky session creation: TooManyRedirects")
                logging.warning (TMROops)
            except requests.exceptions.RequestException as ROops:
                logging.warning ("Bluesky session creation: Generic RequestException")
                logging.warning (ROops)

        # Log the response
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Sending Bluesky message to " +
                              bsa +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTobluesky

#
# class YearDB -- A class to maintain the year's database of sunset
#                 detail information. It's core instance attribute is
#                 a list of SunsetDetails objects, one each for each
#                 day of the operating year.
#
class YearDB(object):

    #
    # getSunsetDetailsByDate -- To return a SunsetDetails object from
    #                           the database relating to the specific
    #                           date parameter.
    #
    # parameters:
    #   theDate -- The date for which the SunsetDetails object is
    #              being queried. A string in YYYY-MM-DD format.
    #
    # return: The SunsetDetails object relating to the date or None if
    #         not in the database.
    #
    def getSunsetDetailsByDate(self, theDate):
        logging.debug ( "SunsetDetails requested for " + theDate )
        testDate = dt.date ( dt.strptime ( theDate, '%Y-%m-%d' ) )
        for ssd in self.yearDB:
            if testDate == ssd.sunsetDate:
                return ssd

        logging.warning ( "SunsetDetails requested for " +
                          theDate + " and None returned." )
        return None

    #
    # Constuctor. Initialise the DB and call the method to populate it.
    #
    # parameters: None
    #
    # return: Nothing
    #
    def __init__(self):
        logging.debug ( "Initialising the YearDB." )
        self.yearDB = []
        self.loadFileData()

    #
    # getYearDB -- A getter for the list. Useful for testing. If it's
    #              empty, populate it first.
    #
    # parameters: None
    #
    # return: The list.
    #
    def getYearDB(self):
        logging.debug ( "Request for the full YearDB." )
        if len(self.yearDB) == 0:
            logging.debug ( "Within request for the full YearDB: " +
                            "DB is not populated, so populating it now." )
            self.loadFileData()
        return self.yearDB

    #
    # loadFileData -- Populate the year database from the database
    #                 file. It reads each line form the database file,
    #                 creates a new SunsetDetails object and adds it
    #                 to the database list.
    #
    # parameters: None
    #
    # return: Nothing
    #
    def loadFileData(self):
        logging.debug ( "Populating YearDB." )
        global runConfig
        with open (runConfig["general"]["filesDir"] + '/' + \
                   runConfig["general"]["yearDBFileName"]) as yearDBInFile:
            for inLine in yearDBInFile:
                self.yearDB.append(SunsetDetails(inLine))

    #
    # writeDataFile -- Save the database to file. It backs the file up
    #                  first and creates and fills a new file. If
    #                  we've turned off the database-writing feature
    #                  in the configuration, it doesn't do any of
    #                  that, and just prints the DB out to stdout.
    #
    # parameters: None
    #
    # return: Nothing
    #
    def writeDataFile(self):
        logging.debug ( "Writing updated YearDB to file." )
        global runConfig
        if runConfig['debugging']['updateDatabase']:
            logging.info ( "Backing up previous file" )
            yearDBFile = runConfig["general"]["filesDir"] + '/' + \
                runConfig["general"]["yearDBFileName"]
            yearDBBackupFile = runConfig["general"]["filesDir"] + \
                '/' + runConfig["general"]["yearDBFileBackupName"]
            os.rename ( yearDBFile, yearDBBackupFile )
            outFile = open ( yearDBFile, 'w' )
        else:
            logging.info ( "Debugging set not to update file, " +
                           "so not doing so." )
        for rec in self.yearDB:
            outLine = rec.str() + '\n'
            if runConfig['debugging']['updateDatabase']:
                outFile.write ( outLine )
            else:
                print(outLine)
# End class YearDB

#
# class SunsetDetails -- A class to maintain all the information
#                        relating to a single sunset event. The
#                        instance attributes maintained in this class
#                        are:
#                        - Core attributes:
#                          + The date the object refers to.
#                          + The time of the sunset in HH:MM:SS
#                            format. This is the standard time, and
#                            doesn't cater for daylight savings.
#                          + The length of the sthretch in H:MM:SS
#                            format.
#                          + Whether the date is during daylight
#                            savings
#                          + The daylight savings sunset time in
#                            HH:MM:SS format (which is the same as the
#                            sunset time if the date is not in
#                            daylight savings.)
#                          + Whether the sthretch for that date has
#                            been posted.
#                        - Other attributes:
#                          + Today's date -- for convenience.
#                          + Whether the object refers to tomorrow
#                          + The text expressing the length of the
#                            stretch
#                          + The text expressing the sunset time.
#                          + Whether this is a past date that hasn't
#                            been posted.
#                          + The message to be posted
#
class SunsetDetails(object):
    """A class for managing information on a particular sunset"""

    #
    # str -- To convert an instance into a string object for printing
    #        and writing.
    #
    # parameters: None
    #
    # return: A comma-separated string containing the following
    #         details:
    #         - The date the object refers to in YYYY-MM-DD format.
    #         - The time of the sunset in HH:MM:SS format. This is the
    #           standard time, and doesn't cater for daylight savings.
    #         - The length of the sthretch in H:MM:SS format.
    #         - Whether the date is during daylight savings ('Y' or
    #           'N')
    #         - The daylight savings sunset time (which is the same as
    #           the sunset time of the date is not in daylight
    #           savings.)
    #         - Whether the sthretch for that date has been posted.
    #
    def str(self):
        logging.debug ( "Converting this SunsetDetails object to a string" )
        stringToReturn = "" + self.sunsetDate.isoformat() + "," + \
                         self.sunsetTime + "," + \
                         self.sthretch + ","

        if self.inDST:
            stringToReturn = stringToReturn + "Y" + ","
        else:
            stringToReturn = stringToReturn + "N" + ","

        stringToReturn = stringToReturn + self.dstSunsetTime + ","

        if self.posted:
            stringToReturn = stringToReturn + "Y"
        else:
            stringToReturn = stringToReturn + "N"

        return stringToReturn

    #
    # Constuctor. Convert the comma-separated values maintained in the
    #             database file into their various attributes, and
    #             build up all te other relevant attributes from those
    #             details.
    #
    # parameters:
    #   fileLine -- The record relating to this date in the database
    #               file.
    #
    # return: Nothing
    #
    def __init__ (self, fileLine):

        logging.debug ( "Creating a new SunsetDetails object for " + \
                        fileLine )

        global runConfig

        self.todayDate = runConfig["general"]["nowDate"]

        # Remove the trailing new line and then split the line on the
        # commas
        lineList = fileLine.strip('\n').split(',')

        # Convert the date from YYYY-MM-DD into a date object.
        self.sunsetDate = dt.date ( dt.strptime ( lineList[0], '%Y-%m-%d' ) )
        # The sunset time is left as the string it's maintained as in
        # the file.
        self.sunsetTime = lineList[1]
        # The stretch time is left as the string it's maintained as in
        # the file.
        self.sthretch = lineList[2]
        # Are we in Daylight Savings Time?
        if lineList[3] == "Y":
            self.inDST = True
        else:
            self.inDST = False
        # The sunset time in daylight savings time is left as the
        # string it's maintained as in the file.
        self.dstSunsetTime = lineList[4]
        # Has the sthretch for this date already been posted?
        if lineList[5] == "Y":
            self.posted = True
        else:
            self.posted = False

        # Is this date referring to tomorrow?
        if (self.sunsetDate - self.todayDate).days == 1:
            self.tomorrowDate = True
        else:
            self.tomorrowDate = False

        # Split the stretch time into its hour, minute and seconds
        # components.
        lineSthretchList = self.sthretch.split(':')

        # Create a string to express the length of the sthretch in
        # human-readable format.
        if lineSthretchList[2] == "01":
            sthretchSecondText = " and 1 sec"
        else:
            sthretchSecondText = " and " + lineSthretchList[2] + " secs"
        if lineSthretchList[1] == "01":
            sthretchMinuteText = "1 min"
        else:
            sthretchMinuteText = lineSthretchList[1] + " mins"
        if lineSthretchList[0] == "0":
            sthretchHourText = ""
        else:
            if lineSthretchList[0] == "1":
                sthretchHourText = "1 hour, "
            else:
                sthretchHourText = lineSthretchList[0] + " hours, "
        self.sthretchText = sthretchHourText + \
            sthretchMinuteText + sthretchSecondText

        # Create a string to express the sunset time in a
        # quasi-human-readable format.
        if self.inDST:
            self.sunsetTimeExpression = self.sunsetTime + \
                " (" + self.dstSunsetTime + " with DST!)"
        else:
            self.sunsetTimeExpression = self.sunsetTime

        # If the sthretch for this date hasn't been posted and if this
        # date it before today, then we're late on this one.
        if not self.posted and self.sunsetDate < self.todayDate:
            self.unpostedPast = True
        else:
            self.unpostedPast = False

        if not self.posted:
            logging.debug ( "This sthretch hasn't been posted " +
                            self.sunsetDate.isoformat() + " " +
                            self.todayDate.isoformat() )

        # Build a different message for a catch-up post than for the
        # normal one. Using the template text maintained in the
        # configuration file.
        if self.unpostedPast:
            self.messageToPost = runConfig["messages"]["catchup"].replace('%%lineDate%%', self.sunsetDate.isoformat()).replace('%%lineSthretch%%', self.sthretch).replace('%%lineSunsetTimeExpression%%', self.sunsetTimeExpression)
        else:
            self.messageToPost = runConfig["messages"]["normal"].replace('%%lineDate%%', self.sunsetDate.isoformat()).replace('%%sthretchText%%', self.sthretchText).replace('%%lineSunsetTimeExpression%%', self.sunsetTimeExpression)

    #
    # postMessage -- To post the message to the configured
    #                microblogging service.
    #
    # parameters: None
    #
    # return: True if the message was posted successfully, False if
    #         not.
    #
    def postMessage(self):

        logging.info ( "Request to post message for " +
                       self.sunsetDate.isoformat() )
        logging.debug ( self.str() )

        # Don't do anything and return False if this SunsetDetail has
        # already been posted.
        if self.posted:
            logging.warning ( "SunsetDetails object for " +
                              str(self.sunsetDate) +
                              " has been posted already. " +
                              "Not doing anything." )
            return False

        # Local variable intialisation
        postSuccess = False
        configgedTargets = runConfig["general"]["messagingSystems"].split(',')
        socialResponse = None
        fullPostSuccess=""

        logging.debug(str(configgedTargets))
        for target in configgedTargets:
            logging.debug("target: " + target)
            if globals()['postTo' + target](self.messageToPost):
                logging.info("Posting to " + target + " was successful")
                fullPostSuccess = fullPostSuccess + "Y"
            else:
                logging.info("Posting to " + target + " was NOT successful after " + str( runConfig["general"]["retries"] ) + " attempts")
                fullPostSuccess = fullPostSuccess + "N"

        if not runConfig["debugging"]["postMessage"]:
            logging.info ( "As noted, we're not posting anywhere, but this is what would have been posted:" )
            print(self.messageToPost)

        # Note the successful posting.
        if fullPostSuccess.__contains__("Y") and fullPostSuccess.__contains__("N"):
            logging.warning ( "Posting was *somewhat* successful." )
            tmpFullPostSuccess = fullPostSuccess
            for target in configgedTargets:
                logging.info ( str(target) + ": " + tmpFullPostSuccess[0] )
                tmpFullPostSuccess = tmpFullPostSuccess[1:]
            self.posted = True
        elif fullPostSuccess.__contains__("Y"):
            logging.info ( "Posting was successful." )
            self.posted = True
        else: # fullPostSuccess is only Ns
            logging.error ( "Posting was not successful." )

        # If we're posting a catch-up message, wait a while so it
        # doesn't look like we're spamming.
        if self.unpostedPast and self.posted:
            logging.info ( "Waiting " +
                           str (runConfig["general"]["catchupWait"]) +
                           " seconds..." )
            time.sleep(runConfig["general"]["catchupWait"])
            logging.debug ( "Done waiting." )

        if self.posted:
            return True
        else:
            return False

    #
    # setCron -- To set a crontab entry for this object.
    #
    # parameters:
    #   catchupPosts -- the number of previous sthretches that have
    #                   not been posted.
    #
    # return: True if the crontab entry was set successfully. False
    #         otherwise.
    #
    def setCron(self, catchupPosts):

        logging.info ( "Setting cron for " + self.sunsetDate.isoformat() )
        # Do nothing if this SunsetDetail doesn't pertain to tomorrow.
        if not self.tomorrowDate:
            logging.warning ( "SunsetDetails object " + self.sunsetDate +
                              " is not for tomorrow. Not setting cron." )
            return False
        else:
            catchups = catchupPosts
            waitInMins = int(runConfig["general"]["catchupWait"] / 60)
            logging.debug ( "Catchups: " + str ( catchups ) +
                            "; wait between in minutes: " +
                            str ( waitInMins ) )

            # Split the time of the next sunset into hours, minutes
            # (and seconds). Convert the former 2 to ints.
            nextJobTimeList=self.dstSunsetTime.split(':')
            nextJobTimeList[0] = int ( nextJobTimeList[0] )
            nextJobTimeList[1] = int ( nextJobTimeList[1] )

            # If we have any catchups to do tomorrow, cater for this
            # and the wait-times to ensure that tomorrow's post goes
            # out at the correct time.
            while catchups > 0:
                nextJobTimeList[1] = nextJobTimeList[1] - waitInMins
                if nextJobTimeList[1] < 0:
                    nextJobTimeList[1] = nextJobTimeList[1] + 60
                    nextJobTimeList[0] = nextJobTimeList[0] - 1
                catchups = catchups - 1

            # Prep the cron entry.
            userCron = CronTab ( user=True )
            nextJob = \
                userCron.new ( command=runConfig["general"]["fullCommand"] )
            nextJob.minute.on ( nextJobTimeList[1] )
            nextJob.hour.on ( nextJobTimeList[0] )
            nextJob.day.on ( self.sunsetDate.day )
            nextJob.month.on ( self.sunsetDate.month )

            # Set the cron entry unless debugging for this is turned
            # on.
            if runConfig['debugging']['setCron']:
                logging.info ( "Writing cron entry to the crontab: " +
                               str ( nextJob ) )
                userCron.write()
                return True
            else:
                logging.info ( "Not writing cron entry to the crontab " +
                               "due to debugging for this turned on: " +
                               str ( nextJob ) )
                print(str ( nextJob ))
                return False

# End class SunsetDetails

# This is how main() is called. I can't explain it.
if __name__ == '__main__':

    # Build the configuration dictionary for this script.
    thisScript=os.path.realpath(__file__)
    buildRunConfig(thisScript)

    # Prepare the year database
    theYearDB = YearDB()

    # Today's SunsetDetails object
    todaySsd = \
        theYearDB.getSunsetDetailsByDate(runConfig["general"]["nowDate"].isoformat())

    # Tomorrow's SunsetDetails object
    tomorrowSsd = \
        theYearDB.getSunsetDetailsByDate((todaySsd.sunsetDate +
                                          td(days = 1)).isoformat())

    # Yesterday's SunsetDetails object
    pastSsd = \
        theYearDB.getSunsetDetailsByDate((todaySsd.sunsetDate -
                                          td(days = 1)).isoformat())

    # If yesterday's sthretch hasn't been posted, capture it, and keep
    # checking the previous days until we encounter one that *has*
    # been.
    unpostedPasts = {}
    while pastSsd is not None and pastSsd.unpostedPast:
        unpostedPasts[pastSsd.sunsetDate] = pastSsd
        pastSsd = \
            theYearDB.getSunsetDetailsByDate((pastSsd.sunsetDate -
                                              td(days = 1)).isoformat())

    postFailure = False
    # Post all the previous unposted sthretches.
    for unpostedPastDate in sorted(unpostedPasts):
        if unpostedPasts[unpostedPastDate].postMessage() or \
           unpostedPasts[unpostedPastDate].posted:
            del unpostedPasts[unpostedPastDate]
        else:
            postFailure = True
            # Once there's a failure, we don't want to proceed.
            break

    logging.info ( "PostFailure after the catchups? " + str ( postFailure ) )
    # Post today's sthretch if all the previous sthretches (if any)
    # have been posted successfully.
    if postFailure or ( not todaySsd.postMessage() and not todaySsd.posted ):
        unpostedPasts[todaySsd.sunsetDate] = todaySsd

    # Write the updated YearDB
    theYearDB.writeDataFile()

    logging.debug ( "No. catchups to go through tomorrow: " +
                    str (len(unpostedPasts)) )

    # Set tomorrow's cron, taking into consideration how many prior
    # dates haven't been posted previously
    tomorrowCronIsSet = tomorrowSsd.setCron(len(unpostedPasts))

    logging.info ( "Complete. Tomorrow's cron set: " +
                   str ( tomorrowCronIsSet ) )
