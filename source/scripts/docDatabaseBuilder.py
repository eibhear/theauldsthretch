#!/usr/bin/python3
# coding=utf-8

# theauldsthretch -- Posting the length of the grand auld stretch in
#                   the evening all year 'round.

# Copyright 2017-2019 Éibhear Ó hAnluain

# This file is part of theauldsthretch.
#
# theauldsthretch is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# theauldsthretch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# SPDX-FileCopyrightText: 2017-2019 Éibhear Ó hAnluain <eibhear.geo@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# ####################################################### #
#                                                         #
# A script to build a database for a year following the   #
# previous year's winter solstice                         #
#                                                         #
# ####################################################### #

import datetime as dt
from astral import LocationInfo, sun, geocoder, location
import argparse
import configparser, os, sys

# Environment setting
thisScript=os.path.realpath(__file__)
scriptDir=os.path.dirname(thisScript)
instDir=os.path.dirname(scriptDir)
filesDir=instDir + '/files'
configDir=instDir + '/config'
logDir=instDir + '/log'

# We get our information from the config file, but...
mainConfig=configparser.RawConfigParser()

# ... we allow some overriding of that information.
argParser = argparse.ArgumentParser \
            ( description=\
              "Create the database of sunsets and stretches for a given year and location" )

argParser.add_argument ( "-y", "--year",
                         help="The operating year we're building the database for. Use this option to override the value in your config file",
                         default=0,
                         type=int )
argParser.add_argument ( "-l", "--location",
                         help="Where your vantage point is. Use this option to override the value in your config file",
                         default="OlympusMons" )
argParser.add_argument ( "-d", "--date",
                         help="If you just want details for a specific date, use this option. MM-DD format (requires -y)." )
argParser.add_argument ( "-e", "--emacsdiary",
                         help="Generate output to be saved into an emacs diary file",
                         action="store_true" )

theArgs = argParser.parse_args()

#
# getAndValidateConfigs -- To load the configuration file and to
#                          perform some tests on it to make sure it's
#                          sane.
#
# parameters:
#   None
#
def getAndValidateConfigs():

  # Read the config file
  mainConfig.read( configDir + '/theauldsthretch.config')

  if ( theArgs.year is not None ) and ( theArgs.year != 0 ):
    mainConfig.set ( 'general', 'operatingYear', theArgs.year )

  if ( theArgs.location is not None ) and ( theArgs.location != 'OlympusMons' ):
    mainConfig.set ( 'database', 'city_name', theArgs.location )

  mainConfig.set ( 'database', 'specific_date', None )
# End getAndValidateConfigs()

#
# earliestSunsetDateAndTime -- To calculate the date and time of the
#                              earliest sunset for the operating year.
#
# parameters:
#   opYear     -- The operating year. The earliest sunset will be close to
#                 the winter solstice of the previous year.
#   opLocation -- The operating location. Needed to calculate the sunset times.
#
# return: a datetime.datetime object with the date and time of the
#         earliest sunset.
#
def earliestSunsetDateAndTime ( opYear, opLocation ):
  # We're working on the winter solstice immediately preceding the
  # operating year.
  testYear = opYear - 1

  # Pick two dates in September, and get their sunsets
  testDate1 = dt.date ( ( testYear ), 9, 20 )
  testDate2 = dt.date ( ( testYear ), 9, 21 )
  sunset1 = opLocation.sunset ( date = testDate1, local = False )
  sunset2 = opLocation.sunset ( date = testDate2, local = False )

  # If the first date's sunset is earlier than the second date's,
  # we're in the Southern hemisphere.
  solsticeMonth = 0
  dayCount = 0
  if sunset1.time() < sunset2.time():
    # Southern hemisphere
    solsticeMonth = 6
    dayCount = 30
  else:
    # Northern hemisphere
    solsticeMonth = 12
    dayCount = 31

  # We'll start on the first day of the month the solstice and test
  # throughout that whole month.
  testDate = dt.date ( testYear, solsticeMonth, 1 )
  endTestDate = testDate + dt.timedelta ( days = dayCount )

  # Initialising
  tSunset = opLocation.sunset(date = testDate, local = False)
  tSunsetTime = tSunset.time()
  tEarliestSunsetTime = tSunsetTime
  tEarliestSunsetDateAndTime = tSunset

  # As we work through the month, capture the date and the sunset
  # only if the sunset is earlier (or the same as) the previous
  # day's.
  while testDate <= endTestDate:
    testDate = testDate + dt.timedelta ( days = 1 )
    tSunset = opLocation.sunset(date = testDate, local = False)
    tSunsetTime = tSunset.time()
    if tSunsetTime <= tEarliestSunsetTime:
      tEarliestSunsetTime = tSunsetTime
      tEarliestSunsetDateAndTime = tSunset

  # That's the date to return.
  return tEarliestSunsetDateAndTime
# End earliestSunsetDateAndTime(opYear,opLocation)

#
# timeDiff -- Calculate the difference between two times
#
# parameters:
#   earlyTime -- The earlier of the two times.
#   lateTime -- The later of the two times.
#
# return: a datetime.datetime object as the difference.
#
def timeDiff ( earlyTime, lateTime ):
  earlyTimeObj = dt.datetime.strptime ( earlyTime, timeFMT )
  lateTimeObj = dt.datetime.strptime ( lateTime, timeFMT )

  retVal = lateTimeObj - earlyTimeObj

  if retVal >= dt.timedelta(0):
    return retVal.__str__()
  else:
    return "-" + abs(retVal).__str__()
# End timeDiff(sunsetTimeUTC)

#
# isLeapYear -- Test whether the given year is a leap year
#
# parameters:
#   lYear -- the year to test, as an int
#
# return: True or False
#
def isLeapYear ( lYear ):
  if ( ( lYear % 400 ) == 0 ):
    return True
  elif ( ( lYear % 100 ) == 0 ):
    return False
  elif ( ( lYear % 4 ) == 0 ):
    return True
  else:
    return False
# End isLeapYear(lYear)

# Read the configuration file.
getAndValidateConfigs()

# Get our standard configs
city_name = mainConfig.get ( 'database', 'city_name' )
timeFMT = mainConfig.get ( 'database', 'timeFMT' )
theYear = int ( mainConfig.get ( 'general', 'operatingYear' ) )

# Set up the Astral object
theLocation = location.Location(geocoder.lookup(city_name, geocoder.database()))
timezone = theLocation.timezone

# Calculate the earliest sunset for us to get the stretch from
theEarliestSunsetDateAndTime = earliestSunsetDateAndTime ( theYear, theLocation )

# The start date is the day after the previous year's earliest sunset
startDate = theEarliestSunsetDateAndTime.date() + dt.timedelta ( days = 1 )

# How many days we're building for. (maxcount == 366 for leap years!)
counter = 0
if isLeapYear(theYear):
  maxcount = 366
else:
  maxcount = 365

timeList = []

while counter < maxcount:
  # Increment the date
  newDate = startDate + dt.timedelta ( days = counter )

  # The record format
  # [ 0: theDate,
  #   1: DST?,
  #   2: Sunrise, UTC
  #   3: Sunrise, local
  #   4: Solar noon, UTC
  #   5: Solar noon, local
  #   6: Sunset, UTC
  #   7: Sunset, local
  #   8: Hours daylight
  #   9: Sthretch
  # ]
  theDateRecord = [ '', '', '', '', '', '', '', '', '', '' ]

  # Get the date's local and UTC times of interest
  utcSunriseString = str(theLocation.sunrise(date = newDate, local = False))
  utcSolarNoonString = str(theLocation.noon(date = newDate, local = False))
  utcSunsetString = str(theLocation.sunset(date = newDate, local = False))

  localSunriseString = str(theLocation.sunrise(date = newDate, local = True))
  localSolarNoonString = str(theLocation.noon(date = newDate, local = True))
  localSunsetString = str(theLocation.sunset(date = newDate, local = True))

  # Split them into strings
  utcSunriseStringList = utcSunriseString.split()
  utcSolarNoonStringList = utcSolarNoonString.split()
  utcSunsetStringList = utcSunsetString.split()
  localSunriseStringList = localSunriseString.split()
  localSolarNoonStringList = localSolarNoonString.split()
  localSunsetStringList = localSunsetString.split()

  # Fill the record with the easy bits
  # [ 0: theDate,
  theDateRecord[0] = utcSunsetStringList[0]

  #   2: Sunrise, UTC
  theDateRecord[2] = utcSunriseStringList[1].split('+')[0].split('.')[0]
  #   3: Sunrise, local
  theDateRecord[3] = localSunriseStringList[1].split('+')[0].split('.')[0]

  #   4: Solar noon, UTC
  theDateRecord[4] = utcSolarNoonStringList[1].split('+')[0].split('.')[0]
  #   5: Solar noon, local
  theDateRecord[5] = localSolarNoonStringList[1].split('+')[0].split('.')[0]

  #   6: Sunset, UTC
  theDateRecord[6] = utcSunsetStringList[1].split('+')[0].split('.')[0]
  #   7: Sunset, local
  theDateRecord[7] = localSunsetStringList[1].split('+')[0].split('.')[0]

  #   8: Hours daylight
  theDateRecord[8] = timeDiff ( theDateRecord[2], theDateRecord[6] )

  #   9: Sthretch
  theDateRecord[9] = timeDiff (str(theEarliestSunsetDateAndTime).split()[1].split('+')[0].split('.')[0], theDateRecord[6] )

  # Get the timezone
  localSunsetStringTimezoneHour = \
                                  localSunsetStringList[1].split('+')[1].split(':')[0]

  # The second field is whether we're in DST
  #   1: DST?,
  if localSunsetStringTimezoneHour != "00":
    theDateRecord[1] = "Y"
  else:
    theDateRecord[1] = "N"

  timeList.append(theDateRecord)

  # Increment the counter so that the date is incremented as we
  # enter back into this loop for the next iteration.
  counter = counter + 1

for dateRecord in timeList:

  if theArgs.emacsdiary:
    tmpMonthNumb=dateRecord[0].split('-')[1]
    tmpMonthName=""
    if tmpMonthNumb == "01":
      tmpMonthName="Jan"
    elif tmpMonthNumb == "02":
      tmpMonthName="Feb"
    elif tmpMonthNumb == "03":
      tmpMonthName="Mar"
    elif tmpMonthNumb == "04":
      tmpMonthName="Apr"
    elif tmpMonthNumb == "05":
      tmpMonthName="May"
    elif tmpMonthNumb == "06":
      tmpMonthName="Jun"
    elif tmpMonthNumb == "07":
      tmpMonthName="Jul"
    elif tmpMonthNumb == "08":
      tmpMonthName="Aug"
    elif tmpMonthNumb == "09":
      tmpMonthName="Sep"
    elif tmpMonthNumb == "10":
      tmpMonthName="Oct"
    elif tmpMonthNumb == "11":
      tmpMonthName="Nov"
    elif tmpMonthNumb == "12":
      tmpMonthName="Dec"
    tmpDate=dateRecord[0].split('-')[2] + ' ' + tmpMonthName + ' ' + dateRecord[0].split('-')[0]
    if dateRecord[1] == 'N':
      print ( tmpDate + ' ' + dateRecord[3].split(':')[0] + ':' +
              dateRecord[3].split(':')[1] + ' Sunrise: ' + dateRecord[2]
             )
      print ( tmpDate + ' ' + dateRecord[7].split(':')[0] + ':' +
              dateRecord[7].split(':')[1] + ' Sunset: ' + dateRecord[6] +
              ', Sthretch: ' + dateRecord[9]
             )
    else:
      print ( tmpDate + ' ' + dateRecord[3].split(':')[0] + ':' +
              dateRecord[3].split(':')[1] + ' Sunrise: ' +
              dateRecord[2] + ' (' + dateRecord[3] + ' local time)'
             )
      print ( tmpDate + ' ' + dateRecord[7].split(':')[0] + ':' +
              dateRecord[7].split(':')[1] + ' Sunset: ' +
              dateRecord[6] + ' (' + dateRecord[7] + ' local time)' +
              ', Sthretch: ' + dateRecord[9]
             )
  else:
    print ( dateRecord[0] + ',' + dateRecord[1] + ',' + dateRecord[2] + ',' +
            dateRecord[3] + ',' + dateRecord[4] + ',' + dateRecord[5] + ',' +
            dateRecord[6] + ',' + dateRecord[7] + ',' + dateRecord[8] + ',' +
            dateRecord[9]
           )
