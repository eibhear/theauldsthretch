#!/usr/bin/python3
# coding=utf-8

# theauldsthretch -- Posting the length of the grand auld stretch in
#                   the evening all year 'round.

# Copyright 2017-2022 Éibhear Ó hAnluain

# This file is part of theauldsthretch.
#
# theauldsthretch is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# theauldsthretch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# SPDX-FileCopyrightText: 2017-2022 Éibhear Ó hAnluain <eibhear.geo@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import argparse, logging, os, configparser
from datetime import datetime as dt
from datetime import timedelta as td
from datetime import timezone as tz
import requests

# We need to catch the message we're posting
argParser = argparse.ArgumentParser \
            ( description=\
              "Send an adhoc-message to all the systems that are being posted to." )


argParser.add_argument ( "-f", "--force",
                         help="Send the message without requesting confirmation",
                         action="store_true" )
argParser.add_argument ( "theMessage",
                         help="The message to post to the supported systems" )

theArgs = argParser.parse_args()

# A dictionary that will contain all the configuration settings, both
# from the config file and from the environment.
runConfig = {}

#
# buildRunConfig -- To build up the runConfig dictionary from the
#                   settings in the configuratoin file and the
#                   environment.
#
# parameters:
#   thisScript -- The full path to the script for which the call is
#                 being made. Normally this actual script, but useful
#                 for testing.
#
# return: Nothing
#
# TODOs:
#   - Maintain a list of the expected settings, and what their
#     datatypes should be
#   - Build a set of sanity checks on config settings
#
def buildRunConfig(thisScript):
    global runConfig
    generalDict = {}
    twitterDict = {}
    mastodonDict = {}
    gnusocialDict = {}
    friendicaDict = {}
    blueskyDict = {}
    debuggingDict = {}

    mainConfig=configparser.RawConfigParser()

    generalDict["fullCommand"] = thisScript
    generalDict["appName"] = 'theauldsthretch'
    generalDict["scriptBaseName"]=\
        os.path.basename(os.path.splitext(thisScript)[0])

    generalDict["scriptDir"]=os.path.dirname(thisScript)
    generalDict["instDir"]=os.path.dirname(generalDict["scriptDir"])
    generalDict["configDir"]=generalDict["instDir"] + '/config'

    # Read the config file
    mainConfig.read( generalDict["configDir"] + '/' +
                     generalDict["appName"] + '.config')

    generalDict["filesDir"]=generalDict["instDir"] + '/' + \
        mainConfig.get('general', 'filesDir')
    generalDict["logDir"]=generalDict["instDir"] + '/' + \
        mainConfig.get('general', 'logDir')

    generalDict["messagingSystems"] = \
        mainConfig.get('general', 'messagingSystems')

    generalDict["logLevel"] =  mainConfig.get('general', 'logLevel')
    generalDict["logFile"] = generalDict["logDir"] + '/' + \
        mainConfig.get('general','logFile')

    generalDict["retries"] =  int ( mainConfig.get('general', 'retries') )

    runConfig["general"] = generalDict

    logging.basicConfig ( filename=runConfig["general"]["logFile"],
                          format='"%(asctime)s","%(name)s",' + \
                          '"%(levelname)s","%(funcName)s","%(message)s"',
                          datefmt='%Y-%m-%d %H:%M:%S',
                          level=runConfig["general"]['logLevel']
    )
    logging.info ( "Logging initiated during buildRunConfig(" + \
                   thisScript + ")" )

    twitterDict["apiKey"] = mainConfig.get('twitter', 'apiKey')
    twitterDict["apiSecret"] = mainConfig.get('twitter', 'apiSecret')
    twitterDict["accessToken"] = mainConfig.get('twitter', 'accessToken')
    twitterDict["accessTokenSecret"] = \
        mainConfig.get('twitter', 'accessTokenSecret')

    runConfig["twitter"] = twitterDict

    mastodonDict["mastodonClientKey"] = mainConfig.get('mastodon', 'mastodonClientKey')
    mastodonDict["mastodonClientSecret"] = mainConfig.get('mastodon', 'mastodonClientSecret')
    mastodonDict["mastodonAccessToken"] = mainConfig.get('mastodon', 'mastodonAccessToken')
    mastodonDict["mastodonApiBaseUrl"] = \
        mainConfig.get('mastodon', 'mastodonApiBaseUrl')

    runConfig["mastodon"] = mastodonDict

    gnusocialDict["gnusocialAddress"] = \
        mainConfig.get('gnusocial', 'gnusocialAddress')
    gnusocialDict["gnusocialUser"] = \
        mainConfig.get('gnusocial', 'gnusocialUser')
    gnusocialDict["gnusocialUserPassword"] = \
        mainConfig.get('gnusocial', 'gnusocialUserPassword')

    runConfig["gnusocial"] = gnusocialDict

    friendicaDict["friendicaAddress"] = \
        mainConfig.get('friendica', 'friendicaAddress')
    friendicaDict["friendicaUser"] = \
        mainConfig.get('friendica', 'friendicaUser')
    friendicaDict["friendicaUserPassword"] = \
        mainConfig.get('friendica', 'friendicaUserPassword')

    runConfig["friendica"] = friendicaDict

    blueskyDict["blueskyAddress"] = \
        mainConfig.get('bluesky', 'blueskyAddress')
    blueskyDict["blueskyUser"] = \
        mainConfig.get('bluesky', 'blueskyUser')
    blueskyDict["blueskyUserPassword"] = \
        mainConfig.get('bluesky', 'blueskyUserPassword')

    runConfig["bluesky"] = blueskyDict

    debuggingDict["postMessage"] = \
        mainConfig.getboolean('debugging', 'postMessage')
    debuggingDict["noChanges"] = \
        mainConfig.getboolean('debugging', 'noChanges')

    runConfig["debugging"] = debuggingDict

    if runConfig['debugging']['noChanges']:
        logging.info ( "Full debugging turned on in config file." )
        runConfig['debugging']['postMessage'] = False

# End buildRunConfig()

def postTotwitter(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Twitter, but was told not to post at all.")
        return True

    logging.info ( "Posting to Twitter." )
    from twython import Twython
    triesToGo = int ( runConfig["general"]["retries"] )
    api = Twython ( runConfig["twitter"]["apiKey"],
                    runConfig["twitter"]["apiSecret"],
                    runConfig["twitter"]["accessToken"],
                    runConfig["twitter"]["accessTokenSecret"] )
    while triesToGo > 0:
        logging.debug ( "Retries left: " + str ( triesToGo ) )
        twitterUpdate = api.update_status ( status=messageToPost )
        logging.info ( "Attempts left: " + str(triesToGo) )

        if twitterUpdate is not None:
            logging.info ( "Twitter response ID: " + twitterUpdate["id_str"] )
            logging.debug ( "Twitter response: " + str ( twitterUpdate ) )
            return True
        else:
            logging.warning ( "Problem posting to twitter" )
            triesToGo = triesToGo -1

    return False
# End of postTotwitter

def postTomastodon(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Mastodon, but was told not to post at all.")
        return True

    logging.info ( "Posting to Mastodon." )
    from mastodon import Mastodon, MastodonError
    triesToGo = int ( runConfig["general"]["retries"] )
    api = Mastodon ( client_id = runConfig["mastodon"]["mastodonClientKey"],
                     client_secret = runConfig["mastodon"]["mastodonClientSecret"],
                     access_token = runConfig["mastodon"]["mastodonAccessToken"],
                     api_base_url = runConfig["mastodon"]["mastodonApiBaseUrl"] )
    while triesToGo > 0:
        logging.debug ( "Retries left: " + str ( triesToGo ) )
        mastodonUpdate = None
        try:
            mastodonUpdate = api.toot ( messageToPost )
        except MastodonError as MOops:
            logging.warning ("Mastodon Error")
            logging.warning (MOops)
        logging.info ( "Attempts left: " + str(triesToGo) )

        if mastodonUpdate is not None:
            logging.info ( "Mastodon response ID: " + str ( mastodonUpdate["id"] ) )
            logging.debug ( "Mastodon response: " + str ( mastodonUpdate ) )
            return True
        else:
            logging.warning ( "Problem posting to Mastodon" )
            triesToGo = triesToGo -1

    return False
# End of postTomastodon

def postTognusocial(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to GNU Social, but was told not to post at all.")
        return True

    logging.info ( "Posting to GNUSocial." )
    gsa = runConfig["gnusocial"]["gnusocialAddress"]
    gsu = runConfig["gnusocial"]["gnusocialUser"]
    gsup = runConfig["gnusocial"]["gnusocialUserPassword"]

    triesToGo = int ( runConfig["general"]["retries"] )

    while triesToGo > 0:
        # Send the message and capture any error.
        socialResponse = None
        try:
            socialResponse = \
                requests.post ( gsa + 'api/statuses/update.xml',
                                auth=( gsu, gsup ),
                                data={ 'status' :
                                       messageToPost }
                )
        except requests.exceptions.ConnectionError as CEOops:
            logging.warning ("Connection Error")
            logging.warning (CEOops)
        except requests.exceptions.Timeout as TOops:
            logging.warning ("Timeout")
            logging.warning (TOops)
        except requests.exceptions.HTTPError as HEOops:
            logging.warning ("HTTPError")
            logging.warning (HEOops)
        except requests.exceptions.TooManyRedirects as TMROops:
            logging.warning ("TooManyRedirects")
            logging.warning (TMROops)
        except requests.exceptions.RequestException as ROops:
            logging.warning ("Generic RequestException")
            logging.warning (ROops)

        # Log the resonse
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Posting to " +
                              gsa +
                              " as " +
                              gsu +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTognusocial

def postTofriendica(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Friendica, but was told not to post at all.")
        return True

    logging.info ( "Posting to friendica." )
    fsa = runConfig["friendica"]["friendicaAddress"]
    fsu = runConfig["friendica"]["friendicaUser"]
    fsup = runConfig["friendica"]["friendicaUserPassword"]
    logging.debug ( "Address: " + fsa + "; User: " + fsu + "; Password: ????" )

    triesToGo = int ( runConfig["general"]["retries"] )

    while triesToGo > 0:
        # Send the message and capture any error.
        socialResponse = None
        try:
            socialResponse = \
                requests.post ( fsa + 'api/statuses/update.xml',
                                auth=( fsu, fsup ),
                                data={ 'status' :
                                       messageToPost }
                )
        except requests.exceptions.ConnectionError as CEOops:
            logging.warning ("Connection Error")
            logging.warning (CEOops)
        except requests.exceptions.Timeout as TOops:
            logging.warning ("Timeout")
            logging.warning (TOops)
        except requests.exceptions.HTTPError as HEOops:
            logging.warning ("HTTPError")
            logging.warning (HEOops)
        except requests.exceptions.TooManyRedirects as TMROops:
            logging.warning ("TooManyRedirects")
            logging.warning (TMROops)
        except requests.exceptions.RequestException as ROops:
            logging.warning ("Generic RequestException")
            logging.warning (ROops)

        # Log the resonse
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Posting to " +
                              fsa +
                              " as " +
                              fsu +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTofriendica

def createBlueskySession():

    logging.info ( "Creating a Bluesky session." )
    bsa = runConfig["bluesky"]["blueskyAddress"]
    bsu = runConfig["bluesky"]["blueskyUser"]
    bsup = runConfig["bluesky"]["blueskyUserPassword"]
    logging.debug ( "Address: " + bsa + "; User: " + bsu + "; Password: ????" )

    sessionRequestResponse = None
    try:
        sessionRequestResponse = \
            requests.post ( bsa + 'xrpc/com.atproto.server.createSession',
                            json={ "identifier":bsu, "password":bsup },
            )
    except requests.exceptions.ConnectionError as CEOops:
        logging.warning ("Bluesky session creation: Connection Error")
        logging.warning (CEOops)
    except requests.exceptions.Timeout as TOops:
        logging.warning ("Bluesky session creation: Timeout")
        logging.warning (TOops)
    except requests.exceptions.HTTPError as HEOops:
        logging.warning ("Bluesky session creation: HTTPError")
        logging.warning (HEOops)
    except requests.exceptions.TooManyRedirects as TMROops:
        logging.warning ("Bluesky session creation: TooManyRedirects")
        logging.warning (TMROops)
    except requests.exceptions.RequestException as ROops:
        logging.warning ("Bluesky session creation: Generic RequestException")
        logging.warning (ROops)

    # Log the response
    if sessionRequestResponse is not None:
        logging.info (sessionRequestResponse.status_code)
        logging.debug (sessionRequestResponse.headers)
        logging.debug (sessionRequestResponse.text)

    # The response may not have resulted in an error and yet not
    # have succeeded. We are successful only if we get a HTTP 200
    # response.
    if sessionRequestResponse is not None and \
       sessionRequestResponse.status_code == 200:
        return sessionRequestResponse.json()
    else:
        return None
# End of createBlueskySession

def postTobluesky(messageToPost):
    if not runConfig["debugging"]["postMessage"]:
        logging.info("Would have posted to Bluesky, but was told not to post at all.")
        return True

    logging.info ( "Posting to bluesky." )
    bsa = runConfig["bluesky"]["blueskyAddress"]
    logging.debug ( "Address: " + bsa )

    triesToGo = int ( runConfig["general"]["retries"] )

    # Create a login session and capture any error that may arise.
    blueskySession = createBlueskySession()

    blueskyPost = { "$type": "app.bsky.feed.post",
                    "text": messageToPost,
                    "createdAt": dt.now(tz.utc).isoformat().replace("+00:00","Z"),
    }

    while triesToGo > 0:

        socialResponse = None
        if blueskySession is not None:
            try:
                socialResponse = \
                    requests.post ( bsa + 'xrpc/com.atproto.repo.createRecord',
                                    headers={ "Authorization": "Bearer " +
                                              blueskySession["accessJwt"]
                                             },
                                    json={ "repo":blueskySession["did"],
                                           "collection":"app.bsky.feed.post",
                                           "record": blueskyPost, },
                    )
            except requests.exceptions.ConnectionError as CEOops:
                logging.warning ("Bluesky session creation: Connection Error")
                logging.warning (CEOops)
            except requests.exceptions.Timeout as TOops:
                logging.warning ("Bluesky session creation: Timeout")
                logging.warning (TOops)
            except requests.exceptions.HTTPError as HEOops:
                logging.warning ("Bluesky session creation: HTTPError")
                logging.warning (HEOops)
            except requests.exceptions.TooManyRedirects as TMROops:
                logging.warning ("Bluesky session creation: TooManyRedirects")
                logging.warning (TMROops)
            except requests.exceptions.RequestException as ROops:
                logging.warning ("Bluesky session creation: Generic RequestException")
                logging.warning (ROops)

        # Log the response
        if socialResponse is not None:
            logging.info (socialResponse.status_code)
            logging.debug (socialResponse.headers)
            logging.debug (socialResponse.text)

        # The response may not have resulted in an error and yet not
        # have succeeded. We are successful only if we get a HTTP 200
        # response.
        if socialResponse is not None and \
           socialResponse.status_code == 200:
            return True
        else:
            # Handling when it isn't successful
            logging.warning ( "Sending Bluesky message to " +
                              bsa +
                              " not successful. See next warning " +
                              "for more information." )
            if socialResponse is not None:
                logging.warning (socialResponse.text)
            else:
                logging.warning ( "socialResponse object is " +
                                  "None -- total connection " +
                                  "failure, it would seem." )
            triesToGo = triesToGo - 1
            logging.warning ( str ( triesToGo ) + " attempts left." )

        sc = None
        if socialResponse is not None:
            sc = str(socialResponse.status_code)
        else:
            sc = "99"
        logging.info ( "Attempts left: " + str(triesToGo) +
                       ", status code: " +
                       sc)
    return False
# End of postTobluesky

#
# postMessage -- To post the message to the configured
#                microblogging service.
#
# parameters: messageToPost -- a string
#
# return: True if the message was posted successfully, False if
#         not.
#
def postMessage(messageToPost):

    logging.info ( "Request to post message for " +
                   messageToPost )

    # Local variable intialisation
    postSuccess = False
    configgedTargets = runConfig["general"]["messagingSystems"].split(',')
    socialResponse = None
    fullPostSuccess=""

    print("The message length is " + str(messageToPost.__len__()) + " characters.")
    if not theArgs.force:
        logging.info("Checking if we should proceed to post")
        print("Proceed? [Y/n] ", end='')
        proceedAns = input()
        logging.debug("proceedAns: " + proceedAns)
        if not (proceedAns == '' or proceedAns == 'y' or proceedAns == 'Y'):
            logging.info("Won't proceed")
            return False
        logging.info("OK. Proceeding to post the ad-hoc message")

    logging.debug(str(configgedTargets))
    for target in configgedTargets:
        logging.debug("target: " + target)
        if globals()['postTo' + target](messageToPost):
            logging.info("Posting to " + target + " was successful")
            fullPostSuccess = fullPostSuccess + "Y"
        else:
            logging.info("Posting to " + target + " was NOT successful after " + str( runConfig["general"]["retries"] ) + " attempts")
            fullPostSuccess = fullPostSuccess + "N"

    if not runConfig["debugging"]["postMessage"]:
        logging.info ( "As noted, we're not posting anywhere, but this is what would have been posted:" )
        logging.info ( messageToPost )
        print("Not posting anything, but here is what would have been posted: \"" + messageToPost + "\"")

    # Note the successful posting.
    if fullPostSuccess.__contains__("Y") and fullPostSuccess.__contains__("N"):
        logging.warning ( "Posting was *somewhat* successful." )
        tmpFullPostSuccess = fullPostSuccess
        for target in configgedTargets:
            logging.info ( str(target) + ": " + tmpFullPostSuccess[0] )
            tmpFullPostSuccess = tmpFullPostSuccess[1:]
        postSuccess = True
    elif fullPostSuccess.__contains__("Y"):
        logging.info ( "Posting was successful." )
        postSuccess = True
    else: # fullPostSuccess is only Ns
        logging.error ( "Posting was not successful." )

    return postSuccess

# This is how main() is called. I can't explain it.
if __name__ == '__main__':

    # Build the configuration dictionary for this script.
    thisScript=os.path.realpath(__file__)
    buildRunConfig(thisScript)

    postMessage(theArgs.theMessage)

    logging.info ( "Message sent." )
