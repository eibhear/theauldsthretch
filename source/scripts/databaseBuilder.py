#!/usr/bin/python3
# coding=utf-8

# theauldsthretch -- Posting the length of the grand auld stretch in
#                   the evening all year 'round.

# Copyright 2017-2019 Éibhear Ó hAnluain

# This file is part of theauldsthretch.
#
# theauldsthretch is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# theauldsthretch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# SPDX-FileCopyrightText: 2017-2019 Éibhear Ó hAnluain <eibhear.geo@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# ####################################################### #
#                                                         #
# A script to build a database for a year following the   #
# previous year's winter solstice                         #
#                                                         #
# ####################################################### #

import datetime as dt
from astral import LocationInfo, sun, geocoder, location
import argparse
import configparser, os, sys

# Environment setting
thisScript=os.path.realpath(__file__)
scriptDir=os.path.dirname(thisScript)
instDir=os.path.dirname(scriptDir)
filesDir=instDir + '/files'
configDir=instDir + '/config'
logDir=instDir + '/log'

# We get our information from the config file, but...
mainConfig=configparser.RawConfigParser()

# ... we allow some overriding of that information.
argParser = argparse.ArgumentParser \
            ( description=\
              "Create the database of sunsets and stretches for a given year and location" )

argParser.add_argument ( "-y", "--year",
                         help="The operating year we're building the database for. Use this option to override the value in your config file",
                         default=0,
                         type=int )
argParser.add_argument ( "-l", "--location",
                         help="Where your vantage point is. Use this option to override the value in your config file",
                         default="OlympusMons" )

# We want the -d and -e arguments to be mutually-exclusive
argDateGroup = argParser.add_mutually_exclusive_group()
argDateGroup.add_argument ( "-d", "--date",
                            help="If you just want details for a specific date, use this option. MM-DD format (requires -y, excludes -e)." )
argDateGroup.add_argument ( "-e", "--earliestsunset",
                            help="If you just want details of the date of the earliest sunset that is the basis of the sthretch calculation for a particular operating year (excludes -d). This option doesn't take a parameter",
                            action="store_true" )

theArgs = argParser.parse_args()

#
# getAndValidateConfigs -- To load the configuration file and to
#                          perform some tests on it to make sure it's
#                          sane.
#
# parameters:
#   None
#
def getAndValidateConfigs():

  # Read the config file
  mainConfig.read( configDir + '/theauldsthretch.config')

  # If we set the operating year at the command-line, capture that.
  if ( theArgs.year is not None ) and ( theArgs.year != 0 ):
      mainConfig.set ( 'general', 'operatingYear', theArgs.year )

  # If we set the location at the command-line, capture that.
  if ( theArgs.location is not None ) and ( theArgs.location != 'OlympusMons' ):
      mainConfig.set ( 'database', 'city_name', theArgs.location )

  # If we set the date at the command-line, capture that.
  if theArgs.date is not None:
      mainConfig.set ( 'database', 'specific_date', theArgs.date )
  else:
      # If we want the detail of the date of the earliest sunset, capture that.
      if theArgs.earliestsunset is True:
          mainConfig.set ( 'database', 'specific_date', "earliestsunset" )
      else:
          mainConfig.set ( 'database', 'specific_date', None )
# End getAndValidateConfigs()

#
# earliestSunsetDateAndTime -- To calculate the date and time of the
#                              earliest sunset for the operating year.
#
# parameters:
#   opYear     -- The operating year. The earliest sunset will be close to
#                 the winter solstice of the previous year.
#   opLocation -- The operating location. Needed to calculate the sunset times.
#
# return: a datetime.datetime object with the date and time of the
#         earliest sunset.
#
def earliestSunsetDateAndTime ( opYear, opLocation ):
    # We're working on the winter solstice immediately preceding the
    # operating year.
    testYear = opYear - 1

    # Pick two dates in September, and get their sunsets
    testDate1 = dt.date ( ( testYear ), 9, 20 )
    testDate2 = dt.date ( ( testYear ), 9, 21 )
    sunset1 = opLocation.sunset ( date = testDate1, local = False )
    sunset2 = opLocation.sunset ( date = testDate2, local = False )

    # If the first date's sunset is earlier than the second date's,
    # we're in the Southern hemisphere.
    solsticeMonth = 0
    dayCount = 0
    if sunset1.time() < sunset2.time():
        # Southern hemisphere
        solsticeMonth = 6
        dayCount = 30
    else:
        # Northern hemisphere
        solsticeMonth = 12
        dayCount = 31

    # We'll start on the first day of the month the solstice and test
    # throughout that whole month.
    testDate = dt.date ( testYear, solsticeMonth, 1 )
    endTestDate = testDate + dt.timedelta ( days = dayCount )

    # Initialising
    tSunset = opLocation.sunset(date = testDate, local = False)
    tSunsetTime = tSunset.time()
    tEarliestSunsetTime = tSunsetTime
    tEarliestSunsetDateAndTime = tSunset

    # As we work through the month, capture the date and the sunset
    # only if the sunset is earlier (or the same as) the previous
    # day's.
    while testDate <= endTestDate:
        testDate = testDate + dt.timedelta ( days = 1 )
        tSunset = opLocation.sunset(date = testDate, local = False)
        tSunsetTime = tSunset.time()
        if tSunsetTime <= tEarliestSunsetTime:
            tEarliestSunsetTime = tSunsetTime
            tEarliestSunsetDateAndTime = tSunset

    # That's the date to return.
    return tEarliestSunsetDateAndTime
# End earliestSunsetDateAndTime(opYear,opLocation)

#
# getSthretch -- Calculate the stretch: the difference between a
#                sunset and the earliest sunset
#
# parameters:
#   sunsetTimeUTC -- The sunset time in UTC as a string in the timeFMT
#                    format.
#
# return: a datetime.datetime object as the difference.
#
def getSthretch ( sunsetTimeUTC ):
    testTime = dt.datetime.strptime ( sunsetTimeUTC, timeFMT )
    # This is embarrassing. Surely there's an easier way to get the
    # difference between a time object and a datetime object!
    theEarliestTime = dt.datetime.strptime ( str(earliestSunsetTime.hour) + ":" +
                                             str(earliestSunsetTime.minute) + ":" +
                                             str(earliestSunsetTime.second),
                                             timeFMT )
    sthretch = testTime - theEarliestTime
    return sthretch
# End getSthretch(sunsetTimeUTC)

#
# isLeapYear -- Test whether the given year is a leap year
#
# parameters:
#   lYear -- the year to test, as an int
#
# return: True or False
#
def isLeapYear ( lYear ):
    if ( ( lYear % 400 ) == 0 ):
        return True
    elif ( ( lYear % 100 ) == 0 ):
        return False
    elif ( ( lYear % 4 ) == 0 ):
        return True
    else:
        return False
# End isLeapYear(lYear)

# Read the configuration file.
getAndValidateConfigs()

# Get our standard configs
city_name = mainConfig.get ( 'database', 'city_name' )
timeFMT = mainConfig.get ( 'database', 'timeFMT' )
theYear = int ( mainConfig.get ( 'general', 'operatingYear' ) )

# Set up the Astral object
theLocation = location.Location(geocoder.lookup(city_name, geocoder.database()))
timezone = theLocation.timezone

# Calculate the earliest sunset for us to get the stretch from
theEarliestSunsetDateAndTime = earliestSunsetDateAndTime ( theYear, theLocation )

# The start date is the day after the previous year's earliest sunset
startDate = theEarliestSunsetDateAndTime.date() + dt.timedelta ( days = 1 )

# If we want the detail of the date of the earliest sunset, specify
# that date, and set the start date to be that date (otherwise, we'd
# overshoot!)
if mainConfig.get('database', 'specific_date') == 'earliestsunset':
    earliestSunsetSpecificDate=str(theEarliestSunsetDateAndTime).split()[0].split('-')[1] + "-" + \
        str(theEarliestSunsetDateAndTime).split()[0].split('-')[2]
    mainConfig.set('database', 'specific_date', earliestSunsetSpecificDate )
    startDate = theEarliestSunsetDateAndTime.date()

# This is the actual time of the sunset on the day of the earliest
# sunset
earliestSunsetTime = theEarliestSunsetDateAndTime.time()

# How many days we're building for. (maxcount == 366 for leap years!)
counter = 0
if isLeapYear(theYear):
    maxcount = 366
else:
    maxcount = 365

while counter < maxcount:
    # Increment the date
    newDate = startDate + dt.timedelta ( days = counter )

    # The record format
    # [ 0: theDate,
    #   1: theSunsetTime,
    #   2: theSthretch,
    #   3: DST?,
    #   4: theSunsetTimeInDST,
    #   5: posted?
    # ]
    theDateRecord = [ '', '', '', '', '', 'N' ]

    # Get the date's local and UTC sunset
    localSunsetString = str(theLocation.sunset(date = newDate, local = True))
    utcSunsetString = str(theLocation.sunset(date = newDate, local = False))

    # Split them into strings
    localSunsetStringList = localSunsetString.split()
    utcSunsetStringList = utcSunsetString.split()

    # The date's the first field and the UTC sunset time is the second
    theDateRecord[0] = utcSunsetStringList[0]
    if mainConfig.get('database', 'specific_date') is not None:
      testDateList = mainConfig.get('database', 'specific_date').split('-')
      todayList = theDateRecord[0].split('-')
      if testDateList[0] != todayList[1] or testDateList[1] != todayList[2]:
        counter = counter + 1
        continue

    theDateRecord[1] = utcSunsetStringList[1].split('+')[0].split('.')[0]

    # Get the timezone
    utcSunsetStringTimezoneHour = \
                                  utcSunsetStringList[1].split('+')[1].split(':')[0]
    localSunsetStringTimezoneHour = \
                                    localSunsetStringList[1].split('+')[1].split(':')[0]

    # Get the stretch
    sthretchDelta = getSthretch ( theDateRecord[1] )

    # The third field is the stretch.
    # If the value is negative, express it as such.
    if sthretchDelta.days < 0:
        sthretchDelta = dt.timedelta ( days=0,
                                       seconds=( 86400 -
                                                 sthretchDelta.seconds
                                               ) )
        theDateRecord[2] = '-' + sthretchDelta.__str__()
    else:
        theDateRecord[2] = sthretchDelta.__str__()

    # The fourth field is whether we're in DST
    if localSunsetStringTimezoneHour != "00":
        theDateRecord[3] = "Y"
    else:
        theDateRecord[3] = "N"

    # The fifth field is the sunset time in DST
    if theDateRecord[3] == "N":
        theDateRecord[4] = theDateRecord[1]
    else:
        theDateRecord[4] = localSunsetStringList[1].split('+')[0].split('.')[0]

    # Print out the information. The sixth record is whether the
    # stretch has been posted, and it defaulted to N above.
    print( theDateRecord[0] + ',' + theDateRecord[1] + ',' +
           theDateRecord[2] + ',' + theDateRecord[3] + ',' + theDateRecord[4]
           + ',' + theDateRecord[5] )

    # Increment the counter so that the date is incremented as we
    # enter back into this loop for the next iteration.
    counter = counter + 1
