#!/usr/bin/python3
# coding=utf-8

# theauldsthretch -- Posting the length of the grand auld stretch in
#                   the evening all year 'round.

# Copyright 2017-2019 Éibhear Ó hAnluain

# This file is part of theauldsthretch.
#
# theauldsthretch is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# theauldsthretch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# SPDX-FileCopyrightText: 2017-2019 Éibhear Ó hAnluain <eibhear.geo@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

from __future__ import print_function
import os, sys

thisScript=os.path.realpath(__file__)
testScriptDir = os.path.dirname(thisScript)
sourceDir = os.path.dirname(testScriptDir)
scriptDir = sourceDir + '/scripts'

sys.path.append(scriptDir)

import postTheSthretch as pts

pts.buildRunConfig(sourceDir + "/scripts/postTheSthretch.py")

myYearDB = pts.YearDB()

for ssd in myYearDB.getYearDB():
  print(ssd.str())

mySsd = myYearDB.getSunsetDetailsByDate("2018-11-30")

mySsd.posted = True

print(mySsd.str())

mySsd = myYearDB.getSunsetDetailsByDate("2018-11-23")

print(mySsd.str())

mySsd = myYearDB.getSunsetDetailsByDate("2018-11-30")

print(mySsd.str())

pts.runConfig["debugging"]["updateDatabase"] = True

myYearDB.writeDataFile()
